import React from 'react';
import ContactForm from './ui/ContactForm';
import ContactItem from './ui/ContactItem';

export default class App extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            listContacts: []
        }
    }

    componentDidMount() {
        let contacts = localStorage.getItem("list-contacts")
        if (contacts != null) {
            this.setState({ listContacts: JSON.parse(contacts) })
        }
    }

    updateList(contact) {
        let contacts = this.state.listContacts
        contacts.push(contact)

        this.setState({ listContacts: contacts })
        localStorage.setItem("list-contacts", JSON.stringify(contacts))
    }

    render() {
        return (
            <div>
                <h1>Contatos</h1>

                <ContactForm onSave={(contact) => this.updateList(contact)} />
                <ul>
                    {
                        this.state.listContacts.map(contact => (
                            <ContactItem key={contact.id} contact={contact} />
                        ))
                    }
                </ul>
            </div>
        )
    }
}