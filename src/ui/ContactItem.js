import React from 'react'

export default class ContactItem extends React.Component{

    constructor(props) {
        super(props)
        this.contact = props.contact
    }

    render() {
        return (
            <li>
                <b>{this.contact.name}</b> <br />
                {this.contact.email} - {this.contact.phone}
            </li>
        )
    }
}