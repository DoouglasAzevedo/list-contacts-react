import React from "react"
import { v4 as uuid } from 'uuid'

export default class ContactForm extends React.Component {

    constructor(props) {
        super(props)
        this.onSave = props.onSave

        this.state = {
            name: '',
            email: '',
            phone: '',
            validation: {
                form: false,
                name: {
                    present: true,
                    format: true
                },
                email: {
                    present: true,
                    format: true
                },
                phone: {
                    present: true,
                    format: true
                }    
            }
        }

        this.frmContact = React.createRef()
        this.refName = React.createRef()
        this.refEmail = React.createRef()
        this.refPhone = React.createRef()
    }
    
    updateField(event, fieldRef) {
        this.validateForm(event, fieldRef)

        let fieldName = event.target.name
        this.setState({ [fieldName] : event.target.value })
    }

    save() {
        let contact = {
            id: uuid(),
            name: this.state.name,
            email: this.state.email,
            phone: this.state.phone
        }

        this.onSave(contact)
        this.setState({
            name: '',
            email: '',
            phone: ''
        })
    }

    validateForm(event, fieldRef) {
        fieldRef.current.checkValidity()

        let fieldValidity = fieldRef.current.validity
        
        let newValidation = this.state.validation
        newValidation[event.target.name].present = !fieldValidity.valueMissing
        newValidation[event.target.name].format = !fieldValidity.typeMismatch
        
        newValidation.formValid = this.frmContact.current.checkValidity()
        
        this.setState({ validation: newValidation })
    }

    render() {

        return (
            <form ref={this.frmContact} noValidate>
                <input name="name" type="text" placeholder="Nome do contato" size="40"
                    value={this.state.name} required ref={this.refName}
                    onChange={ e => this.updateField(e, this.refName)} /> <br />
                
                {!this.state.validation.name.present && <div className="error">Nome é obrigatório!<br /></div>} 

                <input name="email" type="email" placeholder="E-mail do contato" size="25" 
                    value={this.state.email} required ref={this.refEmail}
                    onChange={ e => this.updateField(e, this.refEmail)}/> <br />
                
                {!this.state.validation.email.present && <div className="error">E-mail é obrigatório!<br /></div>}
                {!this.state.validation.email.format && <div className="error">E-mail inválido!<br /></div>}

                <input name="phone" type="text" placeholder="Telefone do contato" 
                    value={this.state.phone} required ref={this.refPhone}
                    onChange={ e => this.updateField(e, this.refPhone)}/> <br />
                
                {!this.state.validation.phone.present && <div className="error">Telefone é obrigatório!<br /></div>}

                <input type="button" value="Salvar" disabled={!this.state.validation.formValid} onClick={() => this.save()} />
            </form>
        )
    }
}